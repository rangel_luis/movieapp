# Movie App

This android application shows you some information regarding hundred movies. The app mainly works with recycler views, interfaces, custom view, animations, Retrofit as HTTP Client, MVVM, caching image, offline mode, video player, pagination and dependency injection.

## Main Activity
This activity handles two recycler views, the first one (horizontal) manages the top rated movies which shows pictures from movies only. The implemented API interface for populating this list is **getTopRated()**. 

The second one (vertical) manages items with more information such as name, release date, duration, time and rating. Here is where **Rating View** was added. The implemented API interface for populating this list is **getMostPopular()**.

This activity works with **w500** size for movie image. 

##### Rating View

The Rating View is a custom view which shows movie ratings. It has **value** and **symbol** as properties. I decided to use canvas for drawing each object of the view. It is composed by 5 objects:

* Background circle
* Ring base
* An arc which depends on ratings
* Text for ratings
* Text for symbol

## Movie Detail Activity

When user touches a movie, this activity is launched. It receives the movie id through intent which will used to get movie data. The implemented API interface for getting movie data is **getMovie(movieId)** which receives movie id parameter and **getVideos(movieId)** for getting video list. Once the activity is loaded, user can play a video.

This activity works with **original** size for movie image. 

## Search Activity

This activity manage a recycler view to show searching results and includes pagination. The implemented API interface for getting searches is **searchMovies(query, page)** which receives two parameters.

## 3rd Party Libraries

I decided to use four 3rd party libraries which helped me to improve some features from the app:

* [Glide](https://github.com/bumptech/glide): I have used this library and I think it's good for caching image.
* [Retrofit](https://square.github.io/retrofit): It's an HTTP client which allows to handle different kinds of HTTP request. From my point of view Retrofit is an excellent library because of versatility.
* [Paginate](https://github.com/MarkoMilos/Paginate): A library for creating pagination functionality.
* [AndroidYoutubePlayer](https://github.com/PierfrancescoSoffritti/android-youtube-player): A library for playing Youtube videos.
