package com.android.movieapp.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "videos")
data class Video (
    @PrimaryKey
    @SerializedName("id")
    @Expose
    val id: String,

    @SerializedName("key")
    @Expose
    val key: String,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("site")
    @Expose
    val site: String,

    var movie_id: Int
)