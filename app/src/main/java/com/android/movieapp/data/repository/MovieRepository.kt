package com.android.movieapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.android.movieapp.data.api.MovieRemoteDataSource
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.api.RetrofitService
import com.android.movieapp.data.local.MovieDao
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val remoteDataSource: MovieRemoteDataSource,
    private val localDataSource: MovieDao
){

    fun getTopRated() = performGetOperation(
        databaseQuery = { localDataSource.getTopRated() },
        networkCall = {  remoteDataSource.getTopRated() },
        saveCallResult = {
            for(movie in it.results!!)
                movie.apply {
                    topRated = true
                    mostPopular = false
                }
            localDataSource.insertAll(it.results)
        }

    )

    fun getMostPopular() = performGetOperation(
        databaseQuery = { localDataSource.getMostPopular() },
        networkCall = { remoteDataSource.getMostPopular() },
        saveCallResult = {
                for(movie in it.results!!)
                    movie.apply {
                        topRated = false
                        mostPopular = true
                    }
                localDataSource.insertAll(it.results) }
    )

    fun getMovie(id : Int) = performGetOperation(
        databaseQuery = { localDataSource.getMovie(id) },
        networkCall = { remoteDataSource.getMovie(id) },
        saveCallResult = { localDataSource.insert(it) }
    )

    fun searchMovies(query : String, page : Int) = RetrofitService.getInstance().searchMovies(query, page)

    private fun <T, A> performGetOperation(databaseQuery: () -> LiveData<T>,
                                           networkCall: suspend () -> Resource<A>,
                                           saveCallResult: suspend (A) -> Unit): LiveData<Resource<T>> =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val source = databaseQuery.invoke().map { Resource.success(it) }
            emitSource(source)

            val responseStatus = networkCall.invoke()
            if (responseStatus.status == Resource.Status.SUCCESS) {

                saveCallResult(responseStatus.data!!)

            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
                emitSource(source)
            }
        }
}