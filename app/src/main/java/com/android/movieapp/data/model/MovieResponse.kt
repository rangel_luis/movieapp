package com.android.movieapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.android.movieapp.data.entities.Movie

class MovieResponse{
    @SerializedName("page")
    @Expose
    val page: Int? = null

    @SerializedName("results")
    @Expose
    val results: List<Movie>? = null

    @SerializedName("total_pages")
    @Expose
    val totalPages: Int? = null
}