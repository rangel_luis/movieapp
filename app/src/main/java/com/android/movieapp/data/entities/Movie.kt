package com.android.movieapp.data.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "top_rated_movies")
data class Movie(
    @PrimaryKey val id: Int,
    val title: String,
    @SerializedName("poster_path")
    @Expose
    val poster: String,
    @SerializedName("release_date")
    @Expose
    val releaseDate: String,
    @SerializedName("vote_average")
    @Expose
    val voteAverage: Float,
    val runtime: Int,
    val overview: String,
    var topRated: Boolean,
    var mostPopular: Boolean
){
    @SerializedName("genres")
    @Expose
    @Ignore var genres: List<Genre>? = null
}