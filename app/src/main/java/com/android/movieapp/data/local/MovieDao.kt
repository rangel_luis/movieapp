package com.android.movieapp.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.movieapp.data.entities.Movie

@Dao
interface MovieDao {
    @Query("SELECT id, '' title, poster, '' releaseDate, 0.0 voteAverage, 0 runtime, '' overview, 0 topRated, 0 mostPopular FROM top_rated_movies WHERE topRated = 1")
    fun getTopRated() : LiveData<List<Movie>>

    @Query("SELECT id, title, poster, releaseDate, voteAverage, 0 runtime, '' overview, 0 topRated, 0 mostPopular FROM top_rated_movies WHERE mostPopular = 1")
    fun getMostPopular() : LiveData<List<Movie>>

    @Query("SELECT * FROM top_rated_movies WHERE id = :id")
    fun getMovie(id: Int): LiveData<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(movies: List<Movie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: Movie)
}