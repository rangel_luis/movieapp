package com.android.movieapp.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.movieapp.data.entities.Video

@Dao
interface VideoDao {
    @Query("SELECT * FROM videos WHERE movie_id = :id")
    fun getVideos(id: Int) : LiveData<List<Video>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(movies: List<Video>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(video: Video)
}