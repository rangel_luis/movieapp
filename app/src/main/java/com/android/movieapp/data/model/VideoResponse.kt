package com.android.movieapp.data.model

import com.android.movieapp.data.entities.Video

data class VideoResponse(val id: Int, val results: List<Video>)