package com.android.movieapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.android.movieapp.data.api.MovieRemoteDataSource
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.local.VideoDao
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class VideoRepository @Inject constructor(
    private val remoteDataSource: MovieRemoteDataSource,
    private val localDataSource: VideoDao
){

    fun getVideos(id : Int) = performGetOperation(
        databaseQuery = { localDataSource.getVideos(id) },
        networkCall = { remoteDataSource.getVideos(id) },
        saveCallResult = {
            for(video in it.results) video.movie_id = id
            localDataSource.insertAll(it.results)
        }
    )

    private fun <T, A> performGetOperation(databaseQuery: () -> LiveData<T>,
                                           networkCall: suspend () -> Resource<A>,
                                           saveCallResult: suspend (A) -> Unit): LiveData<Resource<T>> =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val source = databaseQuery.invoke().map { Resource.success(it) }
            emitSource(source)

            val responseStatus = networkCall.invoke()
            if (responseStatus.status == Resource.Status.SUCCESS) {

                saveCallResult(responseStatus.data!!)

            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
                emitSource(source)
            }
        }
}