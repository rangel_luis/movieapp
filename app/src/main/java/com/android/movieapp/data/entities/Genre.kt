package com.android.movieapp.data.entities

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "genres", primaryKeys = ["id","movie_id"])
data class Genre (
    @SerializedName("id")
    @Expose
    val id: Int,

    @SerializedName("name")
    @Expose
    val name: String,

    var movie_id: Int
)