package com.android.movieapp.data.api

import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(
    private val apiInterface: ApiInterface
): BaseDataSource() {

    suspend fun getTopRated() = getResult { apiInterface.getTopRated() }

    suspend fun getMostPopular() = getResult { apiInterface.getMostPopular() }

    suspend fun getMovie(id : Int) = getResult { apiInterface.getMovie(id) }

    suspend fun getVideos(id : Int) = getResult { apiInterface.getVideos(id) }
}