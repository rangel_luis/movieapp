package com.android.movieapp.data.api

import com.android.movieapp.data.entities.Movie
import com.android.movieapp.data.model.MovieResponse
import com.android.movieapp.data.model.VideoResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("/3/movie/top_rated?api_key=70602560aa3663b3436e64a279a887c5&language=en-US")
    suspend fun getTopRated(): Response<MovieResponse>

    @GET("/3/movie/popular?api_key=70602560aa3663b3436e64a279a887c5&language=en-US")
    suspend fun getMostPopular(): Response<MovieResponse>

    @GET("/3/movie/{movieId}?api_key=70602560aa3663b3436e64a279a887c5&language=en-US")
    suspend fun getMovie(@Path("movieId") movieId: Int): Response<Movie>

    @GET("/3/movie/{movieId}/videos?api_key=70602560aa3663b3436e64a279a887c5&language=en-US")
    suspend fun getVideos(@Path("movieId") movieId: Int): Response<VideoResponse>

    @GET("/3/search/movie?api_key=70602560aa3663b3436e64a279a887c5&language=en-US&include_adult=false&query=&page")
    fun searchMovies(@Query("query") query: String, @Query("page") page: Int): Call<MovieResponse>
}