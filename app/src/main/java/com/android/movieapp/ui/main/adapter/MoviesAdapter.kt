package com.android.movieapp.ui.main.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.movieapp.R
import com.android.movieapp.data.entities.Movie
import com.android.movieapp.ui.custom.RatingView
import com.android.movieapp.ui.main.view.MainActivity
import com.android.movieapp.ui.search.view.SearchActivity
import com.android.movieapp.utils.FunctionHelper
import com.android.movieapp.utils.ObjectHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class MoviesAdapter(private var movies: MutableList<Movie> = ArrayList()) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    fun setMovieList(movies: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    fun addMovieList(movies: List<Movie>) {
        for(movie in movies.toMutableList()) this.movies.add(movie)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(movies[position])

    override fun getItemCount() = movies.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var clMain: ConstraintLayout
        private lateinit var poster: ImageView
        private lateinit var title: TextView
        private lateinit var releaseDate: TextView
        private lateinit var duration: TextView
        private lateinit var rating: RatingView
        private var functionHelper = FunctionHelper()

        fun bind(movie: Movie) = with(itemView) {
            clMain = itemView.findViewById(R.id.clMain)
            poster = itemView.findViewById(R.id.poster)

            poster.layoutParams.width =
                functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.2)!!
            poster.layoutParams.height =
                functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.3)!!


            if(movie.poster != null) {
                val circularProgressDrawable = CircularProgressDrawable(context)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.setColorSchemeColors(Color.WHITE)
                circularProgressDrawable.start()

                Glide
                    .with(this)
                    .load("https://image.tmdb.org/t/p/w500${movie.poster}")
                    .placeholder(circularProgressDrawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transition(DrawableTransitionOptions.withCrossFade(1))
                    .centerCrop()
                    .into(poster)
            }

            title = itemView.findViewById(R.id.title)
            title.text = movie.title

            releaseDate = itemView.findViewById(R.id.releaseDate)
            releaseDate.text = movie.releaseDate?.let { functionHelper.formatDate(it) }

            duration = itemView.findViewById(R.id.duration)
            duration.text = movie.runtime?.let { functionHelper.formatTime(it) }

            rating = itemView.findViewById(R.id.rating)
            movie.voteAverage?.let { (it * 10).toInt() }?.let { rating.setValue(it) }

            clMain.setOnClickListener{
                if(context is MainActivity) (context as MainActivity).onMovieClickListener(movie.id)
                else if(context is SearchActivity) (context as SearchActivity).onMovieClickListener(movie.id)
            }
        }
    }
}