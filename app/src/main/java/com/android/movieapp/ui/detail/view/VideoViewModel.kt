package com.android.movieapp.ui.detail.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.entities.Video
import com.android.movieapp.data.repository.VideoRepository

class VideoViewModel @ViewModelInject constructor(private val videoRepository: VideoRepository) : ViewModel() {
    private val _id = MutableLiveData<Int>()

    private val _videos = _id.switchMap { id -> videoRepository.getVideos(id) }

    val videos: LiveData<Resource<List<Video>>> = _videos

    fun start(id: Int) {
        _id.value = id
    }
}