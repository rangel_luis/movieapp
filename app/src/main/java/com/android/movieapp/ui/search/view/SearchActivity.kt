package com.android.movieapp.ui.search.view

import android.app.ActivityOptions
import android.app.SearchManager
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.transition.Slide
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.movieapp.R
import com.android.movieapp.databinding.ActivitySearchBinding
import com.android.movieapp.ui.detail.view.MovieDetailActivity
import com.android.movieapp.ui.main.adapter.MoviesAdapter
import com.android.movieapp.ui.main.view.MoviesViewModel
import com.android.movieapp.utils.FunctionHelper
import com.android.movieapp.utils.MovieClickListener
import com.android.movieapp.utils.ObjectHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.paginate.Paginate
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class SearchActivity : AppCompatActivity(), Paginate.Callbacks, MovieClickListener {
    private lateinit var binding: ActivitySearchBinding
    private var searchView: SearchView? = null
    private lateinit var moviesAdapter: MoviesAdapter
    private val viewModel: MoviesViewModel by viewModels()
    private var paginate: Paginate? = null
    private var loading = false
    private var page = 0
    private var query = ""
    private var queryWasUpdated = false
    private var totalPages = 0
    private var handler: Handler? = null
    private lateinit var functionHelper : FunctionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            with(window) {
                requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
                enterTransition = Slide().apply { slideEdge = Gravity.RIGHT }
                exitTransition = Slide().apply { slideEdge = Gravity.LEFT }
            }
        }
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView(){
        functionHelper = FunctionHelper()
        functionHelper.initializeAppParams(applicationContext)
        title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            supportActionBar?.elevation = resources.getDimensionPixelSize(R.dimen.toolbarElevation).toFloat()
        binding.ivBackground.layoutParams.width = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.4)!!
        binding.ivBackground.layoutParams.height = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.45)!!
        Glide
            .with(this)
            .load(R.drawable.cinema)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.ivBackground)
        initSearchList()
    }

    private fun initSearchList(){
        binding.recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val itemDecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(this, R.drawable.divider)?.let { itemDecorator.setDrawable(it) }
        binding.recyclerView.addItemDecoration(itemDecorator)
        moviesAdapter = MoviesAdapter()
        binding.recyclerView.adapter = moviesAdapter

        viewModel.search.observe(this, Observer {
            if(!queryWasUpdated){
                if(it.results?.isNotEmpty() == true) {
                    binding.ivBackground.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    totalPages = it.totalPages!!
                    moviesAdapter.addMovieList(it.results)
                }
            }else{
                totalPages = it.totalPages!!
                it.results?.let { it1 ->
                    binding.ivBackground.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                    moviesAdapter.setMovieList(it1)
                    val layoutAnimationController =
                        AnimationUtils.loadLayoutAnimation(applicationContext, R.anim.fall_down)
                    binding.recyclerView.layoutAnimation = layoutAnimationController
                    layoutAnimationController.start()
                }
            }
            queryWasUpdated = false
        })

        viewModel.errorMessage.observe(this, Observer {})

        handler = Handler()

        paginate?.unbind()
        handler?.removeCallbacks(callback)

        paginate = Paginate.with(binding.recyclerView, this)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .setLoadingListItemCreator(null)
            .setLoadingListItemSpanSizeLookup(null)
            .build()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView!!.apply {
            setQuery("",false)
            isFocusable = true
            isIconified = false
            inputType = InputType.TYPE_CLASS_TEXT
            maxWidth = Int.MAX_VALUE
        }

        val closeBtn: ImageView = searchView!!.findViewById<View>(R.id.search_close_btn) as ImageView
        closeBtn.isEnabled = false
        closeBtn.setImageDrawable(null)

        val textView: TextView = searchView!!.findViewById(R.id.search_src_text)
        textView.maxLines = 1
        if (searchManager != null) {
            searchView!!.setSearchableInfo(
                searchManager
                    .getSearchableInfo(componentName)
            )
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                this@SearchActivity.queryWasUpdated = true
                this@SearchActivity.query = query
                this@SearchActivity.page = 1
                viewModel.searchMovies(query, page)
                val imm = this@SearchActivity.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(searchView!!.windowToken, 0);
                return true
            }

            override fun onQueryTextChange(query: String): Boolean {
                return true
            }
        })

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_search) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private val callback = Runnable {
        page++
        viewModel.searchMovies(query, page)
        loading = false
    }

    override fun onLoadMore() {
        if(page != 0) {
            loading = true
            handler?.postDelayed(callback, 2000)
        }else{
            loading = false
        }
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return page == totalPages
    }

    override fun onMovieClickListener(id: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            startActivity(Intent(this@SearchActivity, MovieDetailActivity::class.java).putExtra("movieId", id), ActivityOptions.makeSceneTransitionAnimation(this@SearchActivity).toBundle())
        else
            startActivity(Intent(this@SearchActivity, MovieDetailActivity::class.java).putExtra("movieId", id))
    }
}