package com.android.movieapp.ui.detail.view

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.text.TextUtils
import android.transition.Explode
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.iterator
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.movieapp.R
import com.android.movieapp.ui.player.view.VideoPlayerActivity
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.entities.Genre
import com.android.movieapp.data.entities.Movie
import com.android.movieapp.data.entities.Video
import com.android.movieapp.databinding.ActivityMovieDetailBinding
import com.android.movieapp.utils.FunctionHelper
import com.android.movieapp.utils.ObjectHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class MovieDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMovieDetailBinding
    private lateinit var functionHelper : FunctionHelper
    private val movieDetailViewModel: MovieDetailViewModel by viewModels()
    private val videoViewModel: VideoViewModel by viewModels()
    private var tinyMargin: Int = 0
    private var mediumMargin: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            with(window) {
                requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
                enterTransition = Explode()
                exitTransition = Explode()
            }
        }
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView(){
        functionHelper = FunctionHelper()
        functionHelper.initializeAppParams(applicationContext)

        tinyMargin = resources.getDimensionPixelSize(R.dimen.tinyMargin)
        mediumMargin = resources.getDimensionPixelSize(R.dimen.mediumMargin)

        binding.toolbar.ivHomeButton.setBackgroundResource(R.drawable.back_icon)
        binding.toolbar.ivHomeButton.setOnClickListener { finish() }

        binding.statusBar.statusBar.layoutParams.height = ObjectHelper.statusBarHeight
        binding.statusBar.statusBar.setBackgroundColor(Color.parseColor("#404040"))
        binding.toolbar.toolbar.setBackgroundColor(Color.parseColor("#404040"))

        binding.poster.layoutParams.width =
            functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.4)!!
        binding.poster.layoutParams.height =
            functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.55)!!

        intent?.getIntExtra("movieId",0)?.let { movieDetailViewModel.start(it) }
        intent?.getIntExtra("movieId",0)?.let { videoViewModel.start(it) }

        initMovieDetail()
        setupVideosObserver()
    }

    private fun initMovieDetail(){
        movieDetailViewModel.movie.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    it.data?.let { it1 -> setMovieDetail(it1) }
                }
                Resource.Status.ERROR ->
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    //Nothing
                }
            }
        })
    }

    private fun setMovieDetail(movie : Movie){
        val circularProgressDrawable = CircularProgressDrawable(this)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorSchemeColors(Color.WHITE)
        circularProgressDrawable.start()

        Glide
            .with(this)
            .load("https://image.tmdb.org/t/p/original${movie.poster}")
            .placeholder(circularProgressDrawable)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transition(DrawableTransitionOptions.withCrossFade(1))
            .centerCrop()
            .into(binding.poster)

        binding.title.text = movie.title

        binding.releaseDate.text =
            "${
                movie.releaseDate?.let { functionHelper.formatDate(it) }
            } - ${
                movie.runtime?.let { functionHelper.formatTime(it) }
            }"

        binding.overview.text = getString(R.string.movie_detail_activity_overview)

        binding.overviewDetail.text = movie.overview
        //setGenres(movie.genres!!)
    }

    private fun setGenres(genres : List<Genre>){
        if(genres.isNotEmpty()) {
            val lp: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            lp.setMargins(tinyMargin, 0, 0, 0)

            for (g in genres) {
                val textView = TextView(applicationContext).apply {
                    layoutParams = lp
                    filters = arrayOf(InputFilter.AllCaps())
                    text = g.name
                    gravity = Gravity.CENTER
                    setBackgroundColor(Color.parseColor("#FFFFFF"))
                    setPadding(tinyMargin, tinyMargin, tinyMargin, tinyMargin)
                    setBackgroundResource(R.drawable.g_border)
                    setTextColor(Color.parseColor("#000000"))
                }

                binding.genres.addView(textView)
            }
        }
    }

    private fun setupVideosObserver() {
        videoViewModel.videos.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    if (binding.videosList.childCount == 0 && !it.data.isNullOrEmpty()) setVideoList(it.data)
                }
                Resource.Status.ERROR ->
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    //Nothing
                }
            }
        })
    }

    private fun setVideoList(videosList : List<Video>){
        binding.videos.visibility = View.VISIBLE
        binding.videosList.removeAllViews()

        val lp2: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        lp2.width = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.7)!!
        lp2.height = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.5)!!
        lp2.setMargins(0, 0, mediumMargin, 0)

        val lp: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        lp.width = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.75)!!
        lp.height = functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.5)!!
        lp.setMargins(0, 0, mediumMargin, 0)
        var index = 0
        for(video : Video in videosList) {
            if (video.site?.toLowerCase(Locale.ROOT).equals("youtube")) {
                val relativeLayout = RelativeLayout(this)
                relativeLayout.layoutParams = lp2

                val linearLayout = LinearLayout(this)
                linearLayout.layoutParams = lp
                linearLayout.orientation = LinearLayout.VERTICAL

                val lpPlayer = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0
                )

                lpPlayer.weight = 0.85f

                val youTubePlayerView = YouTubePlayerView(this)
                youTubePlayerView.layoutParams = lpPlayer
                youTubePlayerView.getPlayerUiController().showFullscreenButton(false)
                youTubePlayerView.getPlayerUiController().showMenuButton(false)
                youTubePlayerView.getPlayerUiController().showYouTubeButton(false)
                youTubePlayerView.getPlayerUiController().showSeekBar(false)
                youTubePlayerView.getPlayerUiController().showCurrentTime(false)
                youTubePlayerView.getPlayerUiController().showVideoTitle(false)
                youTubePlayerView.getPlayerUiController().showDuration(false)

                lifecycle.addObserver(youTubePlayerView)

                linearLayout.addView(youTubePlayerView)

                val lpText = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0
                )

                lpText.weight = 0.15f

                val text = TextView(this)
                text.layoutParams = lpText
                text.text = video.name
                text.gravity = Gravity.CENTER_VERTICAL
                text.ellipsize = TextUtils.TruncateAt.END
                text.maxLines = 1
                text.setTextColor(Color.WHITE)
                linearLayout.addView(text)

                relativeLayout.addView(linearLayout)

                val lpText2: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT
                )
                lpText2.addRule(RelativeLayout.CENTER_IN_PARENT)

                val text2 = TextView(this)
                text2.layoutParams = lpText2
                text2.setOnClickListener {
                    startActivity(Intent(applicationContext, VideoPlayerActivity::class.java).putExtra("key", video.key))
                }

                relativeLayout.addView(text2)

                binding.videosList.addView(relativeLayout)
                youTubePlayerView.addYouTubePlayerListener(object :
                    AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        video.key?.let { youTubePlayer.cueVideo(it, 0f) }
                    }
                })
                if(index++ == 1) break
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        for(view : View in binding.videosList)
            (((view as RelativeLayout).getChildAt(0) as LinearLayout).getChildAt(0) as YouTubePlayerView).release()
        binding.videosList.removeAllViews()
    }
}