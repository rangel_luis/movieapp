package com.android.movieapp.ui.main.adapter

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.movieapp.R
import com.android.movieapp.data.entities.Movie
import com.android.movieapp.ui.detail.view.MovieDetailActivity
import com.android.movieapp.ui.main.view.MainActivity
import com.android.movieapp.utils.FunctionHelper
import com.android.movieapp.utils.ObjectHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class HMoviesAdapter(private var movies: List<Movie> = ArrayList()) :
    RecyclerView.Adapter<HMoviesAdapter.ViewHolder>() {

    fun setMovieList(movies: List<Movie>) {
        this.movies = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item2,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(movies[position])

    override fun getItemCount() = movies.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var rlMain: RelativeLayout
        private lateinit var poster: ImageView
        private var functionHelper = FunctionHelper()

        fun bind(item: Movie) = with(itemView) {
            poster = itemView.findViewById(R.id.poster)

            poster.layoutParams.width =
                functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.3)!!
            poster.layoutParams.height =
                functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.45)!!

            if(item.poster != null) {
                val circularProgressDrawable = CircularProgressDrawable(context)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.setColorSchemeColors(Color.WHITE)
                circularProgressDrawable.start()

                Glide
                    .with(this)
                    .load("https://image.tmdb.org/t/p/w500${item.poster}")
                    .placeholder(circularProgressDrawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transition(DrawableTransitionOptions.withCrossFade(1))
                    .centerCrop()
                    .into(poster)
            }

            rlMain = itemView.findViewById(R.id.rlMain)
            poster.setOnClickListener{

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    context.startActivity(Intent(context, MovieDetailActivity::class.java).putExtra("movieId", item.id), ActivityOptions.makeSceneTransitionAnimation(context as MainActivity).toBundle())
                else
                    context.startActivity(Intent(context, MovieDetailActivity::class.java).putExtra("movieId", item.id))
            }
        }
    }
}