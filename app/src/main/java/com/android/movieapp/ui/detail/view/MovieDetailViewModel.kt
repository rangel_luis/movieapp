package com.android.movieapp.ui.detail.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.entities.Movie
import com.android.movieapp.data.repository.MovieRepository

class MovieDetailViewModel @ViewModelInject constructor(private val movieRepository: MovieRepository) : ViewModel() {
    private val _id = MutableLiveData<Int>()

    private val _movie = _id.switchMap { id ->
        movieRepository.getMovie(id)
    }
    val movie: LiveData<Resource<Movie>> = _movie

    fun start(id: Int) {
        _id.value = id
    }
}