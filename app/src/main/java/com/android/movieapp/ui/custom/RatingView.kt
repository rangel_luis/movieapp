package com.android.movieapp.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.android.movieapp.R

class RatingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr){

    private var value : Int
    private var symbol : String
    private val margin = 10f
    private var backgroundPaint : Paint
    private var basePaint : Paint
    private var mainPaint : Paint
    private var valuePaint : Paint
    private var symbolPaint : Paint

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.RatingView,
            0, 0).apply {

            try {
                value = getInteger(R.styleable.RatingView_value, 0)
                symbol = getString(R.styleable.RatingView_symbol).toString()
            } finally {
                recycle()
            }
        }

        backgroundPaint = Paint().apply {
            color = Color.parseColor("#071b22")
            style = Paint.Style.FILL
        }

        basePaint = Paint().apply {
            color = Color.parseColor("#204528")
            strokeWidth = 12f
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
        }

        mainPaint = Paint().apply {
            strokeWidth = 12f
            style = Paint.Style.STROKE
            isAntiAlias = true
            isDither = true
            strokeCap = Paint.Cap.ROUND
        }

        valuePaint = Paint().apply {
            color = Color.WHITE
            textAlign = Paint.Align.CENTER
            typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD)
        }

        symbolPaint = Paint().apply {
            color = Color.WHITE
            textAlign = Paint.Align.RIGHT
        }
    }

    fun setValue(value: Int) {
        this.value = value
        invalidate()
        requestLayout()
    }

    fun setSymbol(symbol: String) {
        this.symbol = symbol
        invalidate()
        requestLayout()
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val oval = RectF().apply {
            left = margin * 2
            top = margin * 2
            right = width.toFloat() - (margin * 2)
            bottom = height.toFloat() - (margin * 2)
        }

        val degrees = (360f / 100f) * value
        //Add background
        canvas.drawCircle((width/2).toFloat(), (height/2).toFloat(), (width/2).toFloat(), backgroundPaint)
        //Add ring base
        canvas.drawArc(oval, 0f, 360f, false, basePaint)
        //Add progress
        mainPaint.color = Color.parseColor(if(value < 50) "#d1d530" else "#1fd07a")
        canvas.drawArc(oval, 270f, degrees, false, mainPaint)
        //Add value
        valuePaint.textSize = (height/3.25).toFloat()
        val xPos = width / 2
        val yPos = (height / 2 - (valuePaint.descent() + valuePaint.ascent()) / 2).toInt()
        canvas.drawText((value).toString(), xPos.toFloat(), yPos.toFloat(), valuePaint)
        //Add symbol
        symbolPaint.textSize = (height * 0.15).toFloat()
        canvas.drawText(symbol, width - (margin * 3), ((height / 2).toFloat()), symbolPaint)
    }
}