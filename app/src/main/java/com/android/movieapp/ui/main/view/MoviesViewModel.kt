package com.android.movieapp.ui.main.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.android.movieapp.data.api.Resource
import com.android.movieapp.data.entities.Movie
import com.android.movieapp.data.repository.MovieRepository
import com.android.movieapp.data.model.MovieResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback

class MoviesViewModel @ViewModelInject constructor(private val movieRepository: MovieRepository) : ViewModel() {
    val search = MutableLiveData<MovieResponse>()
    val errorMessage = MutableLiveData<String>()

    val topRated: LiveData<Resource<List<Movie>>> = movieRepository.getTopRated()

    val mostPopular: LiveData<Resource<List<Movie>>> = movieRepository.getMostPopular()

    fun searchMovies(query : String, page : Int) {
        val response = movieRepository.searchMovies(query, page)
        response.enqueue(object : Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                search.postValue(response.body())
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}