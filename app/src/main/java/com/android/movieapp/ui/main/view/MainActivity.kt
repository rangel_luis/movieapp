package com.android.movieapp.ui.main.view

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.movieapp.R
import com.android.movieapp.data.api.Resource
import com.android.movieapp.databinding.ActivityMainBinding
import com.android.movieapp.ui.detail.view.MovieDetailActivity
import com.android.movieapp.ui.main.adapter.HMoviesAdapter
import com.android.movieapp.ui.main.adapter.MoviesAdapter
import com.android.movieapp.ui.search.view.SearchActivity
import com.android.movieapp.utils.FunctionHelper
import com.android.movieapp.utils.MovieClickListener
import com.android.movieapp.utils.ObjectHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), MovieClickListener {

    private lateinit var moviesAdapter: MoviesAdapter
    private lateinit var hMoviesAdapter: HMoviesAdapter
    private var firstTime = true
    private lateinit var binding: ActivityMainBinding
    private val moviesViewModel: MoviesViewModel by viewModels()
    private lateinit var functionHelper : FunctionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView(){
        firstTime = true
        functionHelper = FunctionHelper()
        functionHelper.initializeAppParams(applicationContext)
        binding.statusBar.statusBar.setBackgroundColor(Color.parseColor("#212121"))
        binding.toolbar.toolbar.setBackgroundColor(Color.parseColor("#212121"))

        binding.statusBar.statusBar.layoutParams.height = ObjectHelper.statusBarHeight

        binding.toolbar.tvActivityTitle.text = getString(R.string.app_name)
        binding.toolbar.tvActivityTitle.setTextColor(Color.WHITE)
        binding.toolbar.tvActivityTitle.setTextColor(Color.WHITE)
        binding.toolbar.ivActionButton.setBackgroundResource(R.drawable.ic_search)
        binding.toolbar.ivActionButton.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                startActivity(Intent(baseContext, SearchActivity::class.java), ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
            else
                startActivity(Intent(baseContext, SearchActivity::class.java))
        }

        initTopRatedList()
        initMostPopularList()
    }

    private fun initTopRatedList(){
        binding.hRecyclerView.layoutParams.height =
            functionHelper.getSizeByPercentage(ObjectHelper.intWidthFullScreen, 0.45)!!

        hMoviesAdapter = HMoviesAdapter()
        binding.hRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.hRecyclerView.adapter = hMoviesAdapter
        if(moviesViewModel.topRated.hasObservers()) moviesViewModel.topRated.removeObservers(this)
        moviesViewModel.topRated.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    if (hMoviesAdapter.itemCount == 0 && !it.data.isNullOrEmpty()) {
                        hMoviesAdapter.setMovieList(ArrayList(it.data))
                        moviesViewModel.topRated.removeObservers(this)
                    }
                }
                Resource.Status.ERROR ->
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    //Nothing
                }
            }
        })
    }

    private fun initMostPopularList(){
        moviesAdapter = MoviesAdapter()

        val itemDecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(this, R.drawable.divider)?.let { itemDecorator.setDrawable(it) }

        binding.vRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.vRecyclerView.addItemDecoration(itemDecorator)
        binding.vRecyclerView.adapter = moviesAdapter

        if(moviesViewModel.mostPopular.hasObservers()) moviesViewModel.mostPopular.removeObservers(this)
        moviesViewModel.mostPopular.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.vRecyclerView.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    if (moviesAdapter.itemCount == 0 && !it.data.isNullOrEmpty()){
                        moviesAdapter.setMovieList(ArrayList(it.data))
                        moviesViewModel.mostPopular.removeObservers(this)
                        if(firstTime) firstTime = false
                    }
                }
                Resource.Status.ERROR -> {
                    binding.vRecyclerView.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    binding.vRecyclerView.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        moviesViewModel.topRated.removeObservers(this)
        moviesViewModel.mostPopular.removeObservers(this)
    }

    override fun onMovieClickListener(id: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            startActivity(Intent(this@MainActivity, MovieDetailActivity::class.java).putExtra("movieId", id), ActivityOptions.makeSceneTransitionAnimation(this@MainActivity).toBundle())
        else
            startActivity(Intent(this@MainActivity, MovieDetailActivity::class.java).putExtra("movieId", id))
    }
}