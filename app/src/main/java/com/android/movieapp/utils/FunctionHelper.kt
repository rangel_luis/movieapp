package com.android.movieapp.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class FunctionHelper {
    private val timeFormater = SimpleDateFormat("mm")
    private val timeFormater2 = SimpleDateFormat("H'h' m'm'")
    private val formatter = SimpleDateFormat("yyyy-MM-dd")
    private val formatter2 = SimpleDateFormat("MMMM dd, yyyy")
    private var calendar = Calendar.getInstance()

    fun formatDate(date : String) : String {
        try {
            calendar.time = formatter.parse(date)
        }catch (ex : ParseException){
            return "-"
        }
        return formatter2.format(calendar.time)
    }

    fun formatTime(t : Int) : String {
        if(t == 0) return "-"
        val date: Date = timeFormater.parse(t.toString())
        return timeFormater2.format(date)
    }

    fun getSizeByPercentage(size: Int, percentage: Double): Int? {
        return (size * percentage).toInt()
    }

    fun initializeAppParams(context: Context) {
        if (!ObjectHelper.blnAppParamsInitialized) {
            ObjectHelper.blnAppParamsInitialized = true

            val displayMetrics = DisplayMetrics()
            val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowmanager.defaultDisplay?.getMetrics(displayMetrics)
            ObjectHelper.intHeightFullScreen = displayMetrics.heightPixels
            ObjectHelper.intWidthFullScreen = displayMetrics.widthPixels
            ObjectHelper.flDensity = displayMetrics.density
            var statusBarHeight = 0
            val resourceId =
                context.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
                ObjectHelper.statusBarHeight = statusBarHeight
            }
        }
    }
}