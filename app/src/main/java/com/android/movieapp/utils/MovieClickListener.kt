package com.android.movieapp.utils

interface MovieClickListener {
    fun onMovieClickListener(id : Int)
}