package com.android.movieapp.utils

class ObjectHelper {
    companion object {
        var blnAppParamsInitialized = false
        var intWidthFullScreen = 0
        var intHeightFullScreen = 0
        var flDensity = 0f
        var statusBarHeight = 0
    }
}