package com.android.movieapp.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.movieapp.data.repository.MovieRepository
import com.android.movieapp.ui.main.view.MoviesViewModel
import javax.inject.Inject

class MyViewModelFactory @Inject constructor(private val movieRepository: MovieRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MoviesViewModel::class.java)) {
            MoviesViewModel(movieRepository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}