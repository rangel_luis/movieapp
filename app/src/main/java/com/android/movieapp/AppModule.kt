package com.android.movieapp

import android.content.Context
import com.android.movieapp.data.api.ApiInterface
import com.android.movieapp.data.api.MovieRemoteDataSource
import com.android.movieapp.data.local.AppDatabase
import com.android.movieapp.data.local.MovieDao
import com.android.movieapp.data.local.VideoDao
import com.android.movieapp.data.repository.MovieRepository
import com.android.movieapp.data.repository.VideoRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideMovieService(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)

    @Singleton
    @Provides
    fun provideMovieRemoteDataSource(movieService: ApiInterface) = MovieRemoteDataSource(movieService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideMovieDao(db: AppDatabase) = db.movieDao()

    @Singleton
    @Provides
    fun provideVideoDao(db: AppDatabase) = db.videoDao()

    @Singleton
    @Provides
    fun provideGenreDao(db: AppDatabase) = db.genreDao()

    @Singleton
    @Provides
    fun provideMovieRepository(movieRemoteDataSource: MovieRemoteDataSource, localDataSource: MovieDao) = MovieRepository(movieRemoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun provideVideoRepository(movieRemoteDataSource: MovieRemoteDataSource, localDataSource: VideoDao) = VideoRepository(movieRemoteDataSource, localDataSource)
}