package com.android.movieapp

import com.android.movieapp.utils.FunctionHelper
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class FunctionHelperTest {

    private lateinit var functionHelper : FunctionHelper
    private lateinit var calendar : Calendar

    @Before
    fun init(){
        functionHelper = FunctionHelper()
        calendar = Calendar.getInstance()
    }

    @Test
    fun formatDate_isCorrect(){
        val monthDate = SimpleDateFormat("MMMM")
        val monthName: String = monthDate.format(calendar.time)
        Assert.assertEquals("$monthName 01, 2021", functionHelper.formatDate("2021-07-01"))
        Assert.assertEquals("-", functionHelper.formatDate("fdsdfsdf"))
    }

    @Test
    fun formatTime_isCorrect(){
        Assert.assertEquals("1h 35m", functionHelper.formatTime(95))
        Assert.assertEquals("-", functionHelper.formatTime(0))
    }

    @Test
    fun getSizeByPercentage_isCorrect(){
        Assert.assertEquals(500, functionHelper.getSizeByPercentage(1000, 0.5))
    }

}